// 菜单配置
// headerMenuConfig：头部导航配置
// asideMenuConfig：侧边导航配置

const headerMenuConfig = [

];

const asideMenuConfig = [
  {
    name: '主面板',
    path: '/dashboard',
    icon: 'home2',
  },
  {
    name: '图表',
    path: '/chart',
    icon: 'chart1',
    children: [
      {
        name: '组件列表',
        path: '/chart/chart-list',
        authority: 'admin',
      },
    ],
  },
  {
    name: '数据分析',
    path: '/data',
    icon: 'table',
    children: [
      {
        name: '学生消费记录',
        path: '/data/consume-list',
        authority: 'admin',
      },
    ],
  },
];

export { headerMenuConfig, asideMenuConfig };
